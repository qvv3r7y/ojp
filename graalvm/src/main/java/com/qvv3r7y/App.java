package com.qvv3r7y;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        String lang = "llvm";
        String fileName = "./c/cpuinfo.bc";

        Context ctx = Context.newBuilder().allowNativeAccess(true).build();
        File file = new File(fileName);
        Source srcLlvm = Source.newBuilder(lang, file).build();

        Value lib = ctx.eval(srcLlvm);
        Value foo = lib.getMember("get_cpuinfo");
        String s = foo.execute().asString();
        ctx.close();

        System.out.println(s);
    }
}
