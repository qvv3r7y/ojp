#include <stdio.h>
#include <graalvm/llvm/polyglot.h>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>


std::string readFile(const std::string& fileName) {
    std::ifstream f(fileName);
    std::stringstream ss;
    ss << f.rdbuf();
    return ss.str();
}


std::string get_cpuinfo () {
    std::cout << "***Hello from native method***\n";
    std::string s;

    s = readFile("/proc/cpuinfo");
    return s;
}