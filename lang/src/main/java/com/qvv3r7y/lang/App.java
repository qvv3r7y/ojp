package com.qvv3r7y.lang;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class App {
    public static void main(String[] args) {
        String fileName;
        if (args.length < 1) {
            fileName = "examples/TranslitScript.trs";
        } else {
            fileName = args[0];
        }
        String src;
        try {
            src = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        parseFile(src);
    }

    public static void parseFile(String src) {
        TranslitScriptLexer lexer = new TranslitScriptLexer(CharStreams.fromString(src));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TranslitScriptParser parser = new TranslitScriptParser(tokens);
        ParseTree ast = parser.parse();

        System.out.println(ast.toStringTree(parser));

        MyVisitor visitor = new MyVisitor();
        visitor.visit(ast);
    }

}
