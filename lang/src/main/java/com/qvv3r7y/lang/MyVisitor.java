package com.qvv3r7y.lang;

import org.antlr.v4.runtime.RuleContext;

import java.util.HashMap;
import java.util.Map;

public class MyVisitor extends TranslitScriptBaseVisitor<Value> {

    private final Map<String, Value> memory = new HashMap<>();
    private final Map<String, RuleContext> labelCtxs = new HashMap<>();


    @Override
    public Value visitAssignment(TranslitScriptParser.AssignmentContext ctx) {
        String id = ctx.ID().getText();
        Value val = this.visit(ctx.expr());
        return memory.put(id, val);
    }

    @Override
    public Value visitIf_stat(TranslitScriptParser.If_statContext ctx) {
        Value evaluated = this.visit(ctx.expr());
        if (evaluated.asBoolean()) {
            for (TranslitScriptParser.StatementContext stm : ctx.statement()) {
                this.visit(stm);
            }
        }
        return Value.VOID;
    }

    @Override
    public Value visitWhile_stat(TranslitScriptParser.While_statContext ctx) {
        Value val = this.visit(ctx.expr());
        while (val.asBoolean()) {
            for (TranslitScriptParser.StatementContext stm : ctx.statement()) {
                this.visit(stm);
            }
            val = this.visit(ctx.expr());
        }
        return Value.VOID;
    }

    @Override
    public Value visitPrint(TranslitScriptParser.PrintContext ctx) {
        Value value = this.visit(ctx.expr());
        System.out.println(value);

        return value;
    }

    @Override
    public Value visitGoto_op(TranslitScriptParser.Goto_opContext ctx) {
        String label = ctx.ID().getText();
        RuleContext labelCtx = labelCtxs.get(label);
        if (labelCtx != null) {
            return visit(labelCtx);
        }
        return Value.VOID;
    }

    @Override
    public Value visitLabel_op(TranslitScriptParser.Label_opContext ctx) {
        String label = ctx.ID().getText();
        labelCtxs.put(label, ctx.parent);
        return Value.VOID;
    }

    @Override
    public Value visitNotExpr(TranslitScriptParser.NotExprContext ctx) {
        Value value = this.visit(ctx.expr());
        return new Value(!value.asBoolean());
    }

    @Override
    public Value visitMultiplicationExpr(TranslitScriptParser.MultiplicationExprContext ctx) {

        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));
        switch (ctx.op.getType()) {
            case TranslitScriptParser.MUL:
                return new Value(left.asInteger() * right.asInteger());
            case TranslitScriptParser.DIV:
                return new Value(left.asInteger() / right.asInteger());
            default:
                throw new RuntimeException("unknown operator: ");
        }
    }

    @Override
    public Value visitAdditiveExpr(TranslitScriptParser.AdditiveExprContext ctx) {

        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));

        switch (ctx.op.getType()) {
            case TranslitScriptParser.ADD:
                return left.isInteger() && right.isInteger() ?
                        new Value(left.asInteger() + right.asInteger()) :
                        new Value(left.asString() + right.asString());
            case TranslitScriptParser.SUB:
                return new Value(left.asInteger() - right.asInteger());
            default:
                throw new RuntimeException("unknown operator: ");
        }

    }

    @Override
    public Value visitRelationExpr(TranslitScriptParser.RelationExprContext ctx) {
        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));

        switch (ctx.op.getType()) {
            case TranslitScriptParser.LT:
                return new Value(left.asInteger() < right.asInteger());
            case TranslitScriptParser.LE:
                return new Value(left.asInteger() <= right.asInteger());
            case TranslitScriptParser.GT:
                return new Value(left.asInteger() > right.asInteger());
            case TranslitScriptParser.GE:
                return new Value(left.asInteger() >= right.asInteger());
            default:
                throw new RuntimeException("unknown operator: ");
        }
    }

    @Override
    public Value visitParenExpr(TranslitScriptParser.ParenExprContext ctx) {
        return this.visit(ctx.expr());
    }

    @Override
    public Value visitNumAtom(TranslitScriptParser.NumAtomContext ctx) {
        Integer integer = Integer.valueOf(ctx.getText());
        return new Value(integer);
    }

    @Override
    public Value visitIdAtom(TranslitScriptParser.IdAtomContext ctx) {
        String id = ctx.getText();
        Value value = memory.get(id);
        if (value == null) {
            throw new RuntimeException("no such variable: " + id);
        }
        return value;
    }

    @Override
    public Value visitStringAtom(TranslitScriptParser.StringAtomContext ctx) {
        String str = ctx.getText();
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
        return new Value(str);
    }

    @Override
    public Value visitType(TranslitScriptParser.TypeContext ctx) {
        return super.visitType(ctx);
    }
}
