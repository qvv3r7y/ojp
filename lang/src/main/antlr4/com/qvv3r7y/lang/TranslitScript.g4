grammar TranslitScript;

parse
 : statement*
 ;

statement
: assignment
| if_stat
| while_stat
| print
| label_op
| goto_op
;

assignment
: ID ASSIGN expr
| type ID ASSIGN expr
;

if_stat
: IF expr THEN statement+ END
;

while_stat
: WHILE expr THEN statement+ LOOP
;

print
: PRINT expr
;

goto_op
: GOTO ID statement+ label_op
;

label_op
: LABEL ID
;

expr
: NOT expr                          #notExpr
| expr op=(MUL | DIV) expr          #multiplicationExpr
| expr op=(ADD | SUB) expr          #additiveExpr
| expr op=(GT | GE | LT | LE) expr  #relationExpr
| atom                              #atomExpr
;

atom
: LPAREN expr RPAREN    #parenExpr
| NUMBER                #numAtom
| ID                    #idAtom
| STRING_LITERAL        #stringAtom
;

type
: INT
| STRING
;

// Keywords

STRING: 'slovo';
INT: 'tseloe';
IF: 'koli';
NOT: 'ne';
THEN: 'delai';
END: 'konets';
LOOP: 'povtori';
GOTO: 'stupai';
LABEL: 'metka';
WHILE: 'poka';
PRINT: 'pechatai';

// Operators
ASSIGN:             '=';
GT:                 '>';
LT:                 '<';
LE:                 '<=';
GE:                 '>=';

ADD:                '+';
SUB:                '-';
MUL:                '*';
DIV:                '/';

LPAREN: '(';
RPAREN: ')';

WHITESPACE : [ \t\r\n]+ -> skip ;
COMMENT: '//' ~[\r\n]* -> skip ;

ID: Letter LetterOrDigit*;
NUMBER: [0-9]+ ;
STRING_LITERAL: '"' ~["\\\r\n]* '"';

fragment LetterOrDigit
: Letter
| [0-9]
;

fragment Letter
: [a-zA-Z$_]
;