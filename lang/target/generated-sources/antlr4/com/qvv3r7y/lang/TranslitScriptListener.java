// Generated from com/qvv3r7y/lang/TranslitScript.g4 by ANTLR 4.9.3
package com.qvv3r7y.lang;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TranslitScriptParser}.
 */
public interface TranslitScriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(TranslitScriptParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(TranslitScriptParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(TranslitScriptParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(TranslitScriptParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(TranslitScriptParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(TranslitScriptParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#if_stat}.
	 * @param ctx the parse tree
	 */
	void enterIf_stat(TranslitScriptParser.If_statContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#if_stat}.
	 * @param ctx the parse tree
	 */
	void exitIf_stat(TranslitScriptParser.If_statContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#while_stat}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stat(TranslitScriptParser.While_statContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#while_stat}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stat(TranslitScriptParser.While_statContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(TranslitScriptParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(TranslitScriptParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#goto_op}.
	 * @param ctx the parse tree
	 */
	void enterGoto_op(TranslitScriptParser.Goto_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#goto_op}.
	 * @param ctx the parse tree
	 */
	void exitGoto_op(TranslitScriptParser.Goto_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#label_op}.
	 * @param ctx the parse tree
	 */
	void enterLabel_op(TranslitScriptParser.Label_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#label_op}.
	 * @param ctx the parse tree
	 */
	void exitLabel_op(TranslitScriptParser.Label_opContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNotExpr(TranslitScriptParser.NotExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNotExpr(TranslitScriptParser.NotExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicationExpr(TranslitScriptParser.MultiplicationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicationExpr(TranslitScriptParser.MultiplicationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAtomExpr(TranslitScriptParser.AtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAtomExpr(TranslitScriptParser.AtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpr(TranslitScriptParser.AdditiveExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpr(TranslitScriptParser.AdditiveExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRelationExpr(TranslitScriptParser.RelationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRelationExpr(TranslitScriptParser.RelationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(TranslitScriptParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(TranslitScriptParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNumAtom(TranslitScriptParser.NumAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNumAtom(TranslitScriptParser.NumAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterIdAtom(TranslitScriptParser.IdAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitIdAtom(TranslitScriptParser.IdAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterStringAtom(TranslitScriptParser.StringAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitStringAtom(TranslitScriptParser.StringAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link TranslitScriptParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(TranslitScriptParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TranslitScriptParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(TranslitScriptParser.TypeContext ctx);
}