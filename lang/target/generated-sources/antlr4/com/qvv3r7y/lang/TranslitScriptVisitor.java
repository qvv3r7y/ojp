// Generated from com/qvv3r7y/lang/TranslitScript.g4 by ANTLR 4.9.3
package com.qvv3r7y.lang;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TranslitScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TranslitScriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(TranslitScriptParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(TranslitScriptParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(TranslitScriptParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#if_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stat(TranslitScriptParser.If_statContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#while_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stat(TranslitScriptParser.While_statContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(TranslitScriptParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#goto_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGoto_op(TranslitScriptParser.Goto_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#label_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabel_op(TranslitScriptParser.Label_opContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(TranslitScriptParser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicationExpr(TranslitScriptParser.MultiplicationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomExpr(TranslitScriptParser.AtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpr(TranslitScriptParser.AdditiveExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationExpr}
	 * labeled alternative in {@link TranslitScriptParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationExpr(TranslitScriptParser.RelationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(TranslitScriptParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumAtom(TranslitScriptParser.NumAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdAtom(TranslitScriptParser.IdAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link TranslitScriptParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtom(TranslitScriptParser.StringAtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link TranslitScriptParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(TranslitScriptParser.TypeContext ctx);
}