package com.qvv3r7y;
//javap -c -s -verbose Main.class

public class App {
    public static void main(String[] args) {
        if (args.length == 0 || args.length > 2 || args[0].equals("-h") || args[0].equals("--help")) {
            System.out.println("Usage:" +
                    "./App <n> <classname>\n" +
                    " n - Factorial number\n" +
                    " classname - Name of generated java class");
            return;
        }
        int fact = Integer.parseInt(args[0]);
        String classname = args.length > 1 ? args[1] : "fact";
        Fgen fgen = new Fgen(fact, classname);
    }
}
