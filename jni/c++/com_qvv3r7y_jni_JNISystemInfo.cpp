#include "com_qvv3r7y_jni_JNISystemInfo.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

static pair<string, string> split_properties(const string &str, char delim) {
    pair<string, string> prop;
    size_t delim_pos = str.find(delim);
    if (delim_pos == string::npos) return prop;
    prop.first = str.substr(0, delim_pos);
    prop.second = str.substr(delim_pos + 1);
    return prop;
}

static map<string, string> process_cpuinfo(std::vector<pair<string, string>> &proc) {
    map<string, string> info;
    info["CPU(s)"] = "0";
    int num_proc;
    auto it = proc.begin();
    for (int i = 0; it != proc.end(); it++, ++i) {
        string key = it->first;
        if (key.find("processor") != string::npos) {
            num_proc = stoi(it->second);
            info["CPU(s)"] = to_string(stoi(info["CPU(s)"]) + 1);
        }
        if (key.find("model name") != string::npos) {
            info["Model"] = it->second;
        }
        if (key.find("cpu cores") != string::npos) {
            info["CPU Cores"] = it->second;
        }
        if (key.find("cpu MHz") != string::npos) {
            string tmp = "MHz ";
            tmp.append(to_string(num_proc));
            tmp.append(" proc");
            info[tmp] = it->second;
        }
    }
    return info;
}

static jobject generate_java_map(map<string, string> info, JNIEnv *env) {
    jclass jmap = env->FindClass("java/util/HashMap");
    if (jmap == NULL) return NULL;

    jsize map_len = info.size();
    jmethodID init = env->GetMethodID(jmap, "<init>", "(I)V");
    jobject hashMap = env->NewObject(jmap, init, map_len);
    jmethodID put = env->GetMethodID(jmap, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

    auto it = info.begin();
    for (int i = 0; it != info.end(); it++, ++i) {
        env->CallObjectMethod(hashMap, put, env->NewStringUTF(it->first.c_str()), env->NewStringUTF(it->second.c_str()));
    }
    return hashMap;
}

/*
 * Class:     com_qvv3r7y_jni_JNISystemInfo
 * Method:    getCpuInfo
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL Java_com_qvv3r7y_jni_JNISystemInfo_getCpuInfo (JNIEnv *env, jclass thisObj) {
    std::cout << "***Hello from native method***\n";
    std::string s;
    std::vector<pair<string, string>> proc;

    std::ifstream in("/proc/cpuinfo");
    if (in.is_open()) {
        while (getline(in, s)) {
            pair<string, string> prop = split_properties(s, ':');
            proc.push_back(prop);
            }
    }
    in.close();
    map<string, string> info = process_cpuinfo(proc);
    return generate_java_map(info, env);
}
