package com.qvv3r7y.jni;

import java.util.Map;

public class JNISystemInfo {
    static final String LIBRARY = "Native";

    static native Map<String, String> getCpuInfo();

    static {
        System.loadLibrary(LIBRARY);
    }

    static void printCpuInfo() {
        Map<String, String> info = getCpuInfo();
        for (Map.Entry<String, String> entry : info.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            System.out.println(key + "\t:" + value);
        }
    }
}
