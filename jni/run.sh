#!/bin/bash
rm -r ./bin;
javac -d bin/ src/main/java/com/qvv3r7y/jni/JNISystemInfo.java -h ./bin;
cp bin/com_qvv3r7y_jni_JNISystemInfo.h c++/;
#export JAVA_HOME=/YOUR JAVA PATH;
g++ -I"$JAVA_HOME/include/" -I"$JAVA_HOME/include/linux" -fPIC c++/com_qvv3r7y_jni_JNISystemInfo.cpp -shared -o lib/libNative.so -Wl,-soname -Wl,--no-whole-archive;

mvn compile;
java -Djava.library.path=./lib -classpath ./target/classes/ com.qvv3r7y.jni.App;
