package com.qvv3r7y.transformers;

import javassist.*;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

public class MyClassTransformer implements ClassFileTransformer {

    private static int classLoadCount = 0;

    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) {

        ++classLoadCount;
        if (className.equals("java/lang/Shutdown$Lock")) {
            printClassLoadCount();
        }

        if (className.equals("com/qvv3r7y/demo/App")) {
            byte[] byteCode = modifyClass(classfileBuffer, 99);
            return byteCode;
        }

        return classfileBuffer;
    }

    private void printClassLoadCount() {
        //System.out.println("Load class: " + className.replace("/", "."));
        System.out.println("*** Loaded " + classLoadCount + " classes ***");
    }

    private byte[] modifyClass(byte[] classfile, int increase) {
        byte[] byteCode = classfile.clone();

        try {
            ClassPool classPool = ClassPool.getDefault();
            CtClass clazz = classPool.get("com.qvv3r7y.demo.App");
            CtMethod method = clazz.getDeclaredMethod("processTransaction");
            method.insertBefore("txNum += " + increase + ";");

            clazz.addField(new CtField(CtClass.doubleType, "minTime", clazz), "1000.0");
            clazz.addField(new CtField(CtClass.doubleType, "maxTime", clazz), "-1.0");
            clazz.addField(new CtField(CtClass.doubleType, "averageTime", clazz), "0.0");
            method.addLocalVariable("startTime", CtClass.longType);
            method.insertBefore("startTime = System.currentTimeMillis();");

            StringBuilder endBlock = new StringBuilder();

            method.addLocalVariable("endTime", CtClass.longType);
            method.addLocalVariable("opTime", CtClass.doubleType);
            endBlock.append("endTime = System.currentTimeMillis();");
            endBlock.append("opTime = (endTime-startTime)/1000.0;");

            endBlock.append("averageTime+=opTime;\n" +
                    "if(opTime > maxTime) maxTime=opTime;\n" +
                    "if(opTime < minTime) minTime=opTime;\n" +
                    "if(txNum == 108) System.out.println(\"*** Min time: \" + minTime +\"\\n*** Max time: \" + maxTime + \"\\n*** Average time: \" + averageTime/10);");

            method.insertAfter(endBlock.toString());
            byteCode = clazz.toBytecode();
            clazz.detach();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return byteCode;
    }
}
