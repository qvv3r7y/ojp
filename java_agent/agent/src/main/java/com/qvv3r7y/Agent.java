package com.qvv3r7y;

import com.qvv3r7y.transformers.MyClassTransformer;

import java.lang.instrument.Instrumentation;

public class Agent {
    private static Instrumentation instrumentation;

    public static void premain(String agentArgs, Instrumentation inst) {
        instrumentation = inst;
        transformClasses();
    }

    public static void transformClasses() {
        System.out.println("*** Agent Class started ***");
        instrumentation.addTransformer(new MyClassTransformer(), true);
    }
}