package com.qvv3r7y.demo;

public class App {

    public static void main(String[] args) throws Exception {
        App app = new App();
        for (int i = 0; i < 10; i++) {
            app.processTransaction(i);
        }
    }

    public void processTransaction(int txNum) throws Exception {
        System.err.println("Processing tx : " + txNum);
        int sleep = (int) (Math.random() * 1000);
        Thread.sleep(sleep);
        System.err.println(String.format("tx: %d completed", txNum));
    }

}
